Easypost for Laravel
==============

Integrates the Easypost PHP library with Laravel via a ServiceProvider.

### Installation

Require laravel easypost:

~~~
"composer require eyeweb/laravel-easypost"
~~~

If using Laravel 5.5+ the ServiceProvider will be discovered automatically, if not, add the following to your `app/config/app.php` file:

~~~
'providers' => [
    ...
    Eyeweb\LaravelEasypost\LaravelEasypostServiceProvider::class,
]
~~~

### Configuration

Add the following to your `.env` file:
~~~
EASYPOST_API_KEY=key_from_easypost
~~~