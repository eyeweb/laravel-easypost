<?php namespace Eyeweb\LaravelEasypost\Providers;

use EasyPost\EasyPost;
use Illuminate\Support\ServiceProvider;

class LaravelEasypostServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        EasyPost::setApiKey(env('EASYPOST_API_KEY'));
    }

}